g.region rows=50 cols=100 res3=50 t=500 b=0 n=1000 s=0 w=0 e=2000 -p3

r.mapcalc expression="phead = if(col() == 1, 30, 29)" --overwrite
r.mapcalc expression="status = if(col() == 1  || col() == 100, 2, 1)" --overwrite
r.mapcalc expression="top_unconf = 100.0" --overwrite
r.mapcalc expression="null = 0.0" --overwrite
r.mapcalc expression="poros = 0.15" --overwrite
r.mapcalc expression="hydcond = 0.0025" --overwrite

r.gwflow top=top_unconf bottom=null phead=phead status=status hc_x=hydcond hc_y=hydcond s=poros output=gwresult type=unconfined dt=864000000 vx=velX vy=velY error=0.000000000000001 maxit=100000000000 --overwrite

r.mapcalc expression="well = if(col()== 70 && row()==20, -0.1,0)" --overwrite

r.gwflow top=top_unconf bottom=null phead=gwresult status=status q=well hc_x=hydcond hc_y=hydcond s=poros output=pumping type=unconfined dt=864000000 vx=velX vy=velY error=0.000000000000001 maxit=100000000000 --overwrite


r.mapcalc expression="inject = if(col()==80 && row()==35, 0.1,0.0)" --overwrite
r.mapcalc expression="q=inject+well" --overwrite

r.gwflow top=top_unconf bottom=null phead=gwresult status=status q=q hc_x=hydcond hc_y=hydcond s=poros output=pumpInject type=unconfined dt=864000000 vx=velX vy=velY error=0.000000000000001 maxit=100000000000 --overwrite

r.mapcalc "magnitude = sqrt(velX^2 + velY^2)" --overwrite
r.mapcalc "direction = atan(velX, velY)" --overwrite

d.rast.arrow map=direction color=black magnitude_map=magnitude skip=50 grid=none scale=100

########################################################################################
g.region rows=50 cols=100 res3=50 t=500 b=0 n=1000 s=0 w=0 e=2000 -p3

r.mapcalc expression="phead = if(col() == 1, 30, 29)" --overwrite
r.mapcalc expression="status = if(col() == 1  || col() == 100, 2, 1)" --overwrite
r.mapcalc expression="hydcond = 0.0025" --overwrite
r.mapcalc expression="top_unconf = 100.0" --overwrite
r.mapcalc expression="null = 0.0" --overwrite
r.mapcalc expression="poros = 0.15" --overwrite

r.mapcalc expression="well = if(col()== 70 && row()==20, -1,0)" --overwrite

r.gwflow top=top_unconf bottom=null phead=phead status=status hc_x=hydcond hc_y=hydcond s=poros output=gwresult type=unconfined dt=864000000 error=0.000000000000001 maxit=100000000000 --overwrite

r.mapcalc expression="phead_0=gwresult" --overwrite


## Python script
#!/usr/bin/env python3

import grass.script as gscript


def main():
    i=0

    while i<10:
        gscript.run_command('r.gwflow', 
            top="top_unconf",
            bottom="null",
            phead="phead_"+str(i),
            status="status",
            q="well",
            hc_x="hydcond",
            hc_y="hydcond",
            s="poros",
            output="phead_"+str(i+1),
            type="unconfined",
            dt="86400",
            error="0.000000000000001",
            maxit="100000000000",
            overwrite=True)
            
        i=i+1


if __name__ == '__main__':
    main()
